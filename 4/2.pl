#!/usr/bin/perl

use strict;
use Data::Dumper;

open FILE, "input";
my @times = sort <FILE>;
close FILE;

my (%sleepLog,%sleepSum,$hour, $min, $range,$date,$time, $action, $guard,$sumSleep);


while (my $line = shift @times) {
	chomp $line;
	if ($line =~ /\[(\d+-\d+-\d+).*\] Guard #(\d+).*/) {
		#$sleepLog{$guard}{$date}
		$sumSleep = 0;
		$date = $1;
		$guard = $2;
		print "$date Guard $guard starts\n";
		next;
	}	
	
	#sleep
	my ($shour,$smin,$saction) = $line =~ /(\d+):(\d+)\].*(sleep).*/;
	#print $line;
	
	#wake
	my $wake = shift @times;
	chomp $wake;
	my ($whour,$wmin,$waction) = $wake =~ /(\d+):(\d+)\].*(wake).*/;
	#print "$wake\n";
	

	my $sdiff = $wmin-$smin;

	print "$wmin - $smin = $sdiff\n";
	
	$sleepSum{$guard} += $sdiff;

	$sleepLog{$guard} .= $smin . $wmin . ","; 

}

#my @mostSleep = sort { $sleepSum{$a} <=> $sleepSum{$b} } keys %sleepSum;
#my $topSleeper = $mostSleep[-1];
#my @sortedTimes = split /,/, $sleepLog{$topSleeper};

my %one2sixty;

foreach $guard (keys %sleepLog) {
	my @sortedTimes = split /,/, $sleepLog{$guard};

#foreach (sort @sortedTimes) {
#	my ($str,$stop) = $_ =~ /(\d\d)(\d\d)/;
#	foreach my $minute (0..59) {
#		if ($minute >= $str && $minute < $stop) {
#			$one2sixty{$minute}++;
#		}	
#	}
#}
#
#my @bestMinute = sort { $one2sixty{$a} <=> $one2sixty{$b} } keys %one2sixty;
#my $bestTime = $bestMinute[-1];
#my $solution = $topSleeper*$bestTime;
}
#print "$solution\n";
