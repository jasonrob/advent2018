#!/usr/bin/perl

use strict;

my $freq = 0;
my %occur;
my $match;

while (!$match) {
	open FILE, "input" or die $!;
	while (<FILE>){
		$freq += $_;
		if ($occur{$freq}) {
			$match = 'yay';
			print "$freq\n";
			last;
		}
		$occur{$freq} = $freq;
	}
}
