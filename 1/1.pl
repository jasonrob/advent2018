#!/usr/bin/perl

use strict;

open FILE, "input" or die $!;

my $freq = 0;

while (<FILE>){
	$freq += $_;
}

print "$freq\n";
