#!/usr/bin/perl

use strict;

open FILE, "input" or die $!;

my %matches;
my $line;

while (<FILE>) {
	$line = $_;
	my @chars = split //, $line;
	my $match2;
	my $match3;
	foreach my $char (@chars) {
		my $count = () = $line =~ /$char/g;
		if ($count == 2 && !$match2) {
			$matches{'two'}+=1;
			$match2 = "y";	
		} 
		if ($count == 3 && !$match3) {
			$matches{'three'}+=1;
			$match3 = "y";
		}
	}
}

my $solution = $matches{'two'}*$matches{'three'};

print "$solution\n";
