#!/usr/bin/perl

use strict;

open FILE, "input" or die $!;
my @boxid = <FILE>;
close FILE;

my $matchsize = () = $boxid[0] =~ /./g;
$matchsize--; #should be 25
my $nosolution;

while (!$nosolution) {
	my $matchChars = "";
	my $deviation = 0;
	
	my $curLine = pop @boxid;
	chomp $curLine;
	my @curLineChars = split '', $curLine;
	
	foreach my $id (@boxid) {
		chomp $id;
		my @searchLineChars = split '', $id;
		# reset deviation and match chars
		$deviation = 0;
		$matchChars = "";
		foreach (my $i = 0; $i < @searchLineChars; $i++) {
	        	if ($curLineChars[$i] ne $searchLineChars[$i]) {
				$deviation++;
				last if ($deviation > 1);
			} else {
				$matchChars .= $curLineChars[$i];
			}
		}
		my $countMatches = () = $matchChars =~ /./g;
		next unless ($countMatches == $matchsize);
		print "$matchChars\n";
		$nosolution = "screw you day 2.2";
	}
}
