#!/usr/bin/perl

use strict;

open FILE, "input" or die $!;

my @fabric;
my $sum;

while (<FILE>) {
	my ($id, $left, $top, $wid, $tall) = $_ =~ /#(\d+) @ (\d+),(\d+): (\d+)x(\d+)/;

	foreach  (my $t = 0; $t < $tall; $t++) {
		foreach (my $l = 0; $l < $wid; $l++) {
			my $foo = $left+$l;
			$fabric[$left+$l][$top] += 1;
			print "$foo : $top = $fabric[$left+$l][$top]\n";
			if ($fabric[$left+$l][$top] == 2) {
				$sum++;
			}
		}
		$top++;
	}	
}

print $sum;
