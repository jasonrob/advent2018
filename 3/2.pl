#!/usr/bin/perl

use strict;

open FILE, "input" or die $!;

my @fabric;
my $sum;
my $reggie;
my $nolap;

while (<FILE>) {
	my ($id, $left, $top, $wid, $tall) = $_ =~ /#(\d+) @ (\d+),(\d+): (\d+)x(\d+)/;
	foreach  (my $t = 0; $t < $tall; $t++) {
		foreach (my $l = 0; $l < $wid; $l++) {
			my $foo = $left+$l;
			$fabric[$left+$l][$top] += 1;
			if ($fabric[$left+$l][$top] == 2) {
				$sum++;
			}
		}
		$top++;
	}
}

open FILE, "input" or die $!;

while (<FILE>) {
        my ($id, $left, $top, $wid, $tall) = $_ =~ /#(\d+) @ (\d+),(\d+): (\d+)x(\d+)/;
        $reggie ="";
        $nolap = $id;
        foreach  (my $t = 0; $t < $tall; $t++) {
                foreach (my $l = 0; $l < $wid; $l++) {
                        my $foo = $left+$l;
                        $reggie .= $fabric[$left+$l][$top];
                }
                $top++;
        }       
        last if ($reggie =~ /^1+$/);   
}

print $nolap;
